use fnv::{FnvBuildHasher, FnvHashMap};
use log::LevelFilter;
use log4rs::{
    append::file::FileAppender,
    config::{Appender, Config, Root},
    encode::pattern::PatternEncoder,
};
use lsharp_ru::{
    definitions::mealy::{InputSymbol, OutputSymbol},
    learner::{
        l_sharp::{Lsharp, Rule2, Rule3},
        obs_tree::map_tree::ObsTree as ObsMapTree,
    },
    oracles::{
        equivalence::{
            hads::{ConfigBuilder as HadsBuilder, Mode, Prefix, Suffix},
            EquivalenceOracle,
        },
        membership::Oracle as OQOracle,
    },
    sul::Stdio,
    util::writers::overall as MealyWriter,
};
use std::{cell::RefCell, fs, hash::BuildHasherDefault, path::Path, rc::Rc};

mod cli;

fn main() -> Result<(), Box<dyn std::error::Error>> {
    type DefaultFxHasher = BuildHasherDefault<rustc_hash::FxHasher>;
    type WrappedOracle<'a> = Rc<RefCell<OQOracle<'a, ObsMapTree<DefaultFxHasher>>>>;
    ensure_dir_exists("./hypothesis")?;
    let matches = cli::parse()?;
    let input_alphabet: Vec<String> = matches
        // .get_many("ia")
        .values_of("ia")
        .expect("Input alphabet specified incorrectly.")
        .map(|x| x.to_string())
        .flat_map(|s| {
            s.split_ascii_whitespace()
                .map(|x| x.to_string())
                .collect::<Vec<_>>()
        })
        .collect();
    assert!(
        !input_alphabet.is_empty(),
        "Input alphabet should not be empty."
    );

    // Input alphabet is ready.
    let input_map: FnvHashMap<_, _> = input_alphabet
        .into_iter()
        .map(|s| s.trim().to_string())
        .enumerate()
        .map(|(idx, i)| (i, InputSymbol::from(idx)))
        .collect();

    println!("Input map: {:?}", input_map);

    let sul_lib_path = matches
        .value_of("em")
        .expect("SUL path incorectly specified?");
    let sul_lib_path = {
        let path = Path::new(sul_lib_path);
        std::fs::canonicalize(path)?
    };

    let logfile = FileAppender::builder()
        .encoder(Box::new(PatternEncoder::new(
            "{d(%Y-%m-%d %H:%M:%S)} {l} {M} | {m}{n}",
        )))
        .build(format!("log/{}.log", chrono::Utc::now()))?;

    let config = Config::builder()
        .appender(Appender::builder().build("logfile", Box::new(logfile)))
        .build(
            Root::builder()
                .appender("logfile")
                .build(LevelFilter::Debug),
        )?;
    log4rs::init_config(config)?;

    println!("SUL : {}", sul_lib_path.display());
    // Load the DLL and the methods in an SUL.
    // let em = unsafe { libloading::Library::new(sul_lib_path)? };
    // let mut ext_oracle = unsafe {
    //     let awesome: Symbol<'_, unsafe fn() -> *const c_char> = em.get(b"awesome")?;
    //     let resp = CStr::from_ptr(awesome());
    //     println!("{}", resp.to_string_lossy());
    //     let reset_func: Symbol<_> = em.get(b"Reset")?;
    //     let step_func = em.get(b"Step")?;
    //     External::new(step_func, reset_func, &input_map)
    // };

    let mut stdio_oracle =
        Stdio::new(&sul_lib_path, &input_map, "RESET".to_string()).expect("Safe");

    // Make an oracle using the SUL.
    let oq_oracle: WrappedOracle = Rc::new(RefCell::new(OQOracle::new(
        // &mut ext_oracle,
        &mut stdio_oracle,
        Rule2::Ads,
        Rule3::Ads,
        OutputSymbol::new(u16::MAX), // sink output doesn't exist
    )));

    let rand_extra_states = 5; // Expected length of the random sequence.
    let seed = 0; // Random seed to use.

    // Construct an equivalence oracle.
    let mut hads_oracle = HadsBuilder::default()
        .mode(Mode::Random)
        .prefix(Prefix::Buggy)
        .suffix(Suffix::Hads)
        .extra_states(0)
        .expected_rnd_length(rand_extra_states)
        .seed(seed)
        .build()
        .expect("Safe")
        .oracle(
            Rc::clone(&oq_oracle),
            // "/path/to/h-ads/executable/here",
            "/Users/bharat/rust/automata-lib/hybrid-ads/build/main",
        )
        .expect("Could not construct H-ADS oracle.");

    // And Learner.
    let mut learner: Lsharp<
        '_,
        ObsMapTree<BuildHasherDefault<rustc_hash::FxHasher>>,
        FnvBuildHasher,
    > = Lsharp::new(Rc::clone(&oq_oracle), input_map.len(), false);

    let mut idx = 1;
    let mut learn_inputs: usize = 0;
    let mut learn_resets: usize = 0;
    let mut test_inputs: usize = 0;
    let mut test_resets: usize = 0;

    // Set this to whatever you like.
    let max_rounds = 3;

    learner.init_obs_tree(None);

    loop {
        println!("LOOP {}", idx);
        let hypothesis = learner.build_hypothesis();
        let (l_inputs, l_resets) = learner.get_counts();
        learn_inputs += l_inputs;
        learn_resets += l_resets;
        println!("Learning queries/symbols: {}/{}", l_resets, l_inputs,);

        let writer_config = MealyWriter::WriteConfigBuilder::default()
            .file_name(Some(
                format!("./hypothesis/hypothesis_{}.dot", idx).to_string(),
            ))
            .format(MealyWriter::MealyEncoding::Dot)
            .build()
            .expect("Could not write Hypothesis file.");
        {
            let (input_vec, output_vec) = RefCell::borrow(&oq_oracle).pass_maps();
            let rev_input_map = input_vec.into_iter().map(flip).collect();
            let rev_output_map = output_vec.into_iter().map(flip).collect();
            MealyWriter::write_machine::<FnvBuildHasher, _>(
                &writer_config,
                &hypothesis,
                &rev_input_map,
                &rev_output_map,
            )
            .expect("Safe");
        }

        if max_rounds == idx {
            break;
        }

        let ce = hads_oracle.find_counterexample(&hypothesis);
        let (t_inputs, t_resets) = hads_oracle.get_counts();
        test_inputs += t_inputs;
        test_resets += t_resets;
        println!("Testing queries/symbols: {}/{}", t_resets, t_inputs);
        if ce.is_some() {
            println!("CE found, refining observation tree!");
            learner.process_cex(ce, &hypothesis);
        } else {
            println!("No CE found, learning finished.");
            break;
        }
        idx += 1
    }
    println!("MQ [Resets] : {learn_resets}");
    println!("MQ [Symbols] : {learn_inputs}");
    println!("EQ [Resets] : {test_resets}");
    println!("EQ [Symbols] : {test_inputs}");
    println!("Rounds : {idx}");
    Ok(())
}

fn ensure_dir_exists(dir_name: &str) -> std::io::Result<()> {
    // Do not remove any of these lines
    // Remove the dir (returns an error if it doesn't exist)
    // and ignore the error-case.
    let _ = fs::remove_dir_all(dir_name);
    // Create an empty dir.
    fs::create_dir_all(dir_name)
}

fn flip<A, B>((x, y): (A, B)) -> (B, A) {
    (y, x)
}

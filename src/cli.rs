use clap::{value_parser, Arg, ArgAction, ArgMatches, Command};

/// # Errors
/// If no match is made.
pub fn parse() -> Result<ArgMatches, Box<dyn std::error::Error>> {
    let matches = Command::new("L# Learning Example")
        .version("v0.1")
        .author("Bharat Garhewal <bharat.garhewal@ru.nl>")
        .about("Example app for L#, using H-ADS as a CTT and a dynamic library as an SUL.")
        .arg(
            Arg::new("ia")
                .help("The input alphabet.")
                .short('I')
                .value_parser(value_parser!(String))
                .action(ArgAction::Append)
                .takes_value(true)
                .required(true),
        )
        .arg(
            Arg::new("oa")
                .help("The output alphabet.")
                .short('O')
                .multiple_values(true)
                .takes_value(true)
                .required(false),
        )
        .arg(
            Arg::new("em")
                .takes_value(true)
                .short('M')
                .help("Path to the external model file.")
                .required(true),
        )
        .get_matches();
    Ok(matches)
}

#include <string>

using namespace std;
/**
 * @brief Hard-coded three state system, translated from Ramon Janssen's Java version.
 *
 */
class ExampleSUL
{
private:
    enum State
    {
        s0,
        s1,
        s2
    };
    State curr_state = s0;

public:
    ExampleSUL(/* args */);
    void Reset();
    std::string Step(std::string in_symbol);
    ~ExampleSUL();
};
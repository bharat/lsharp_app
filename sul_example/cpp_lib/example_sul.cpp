#include <string>
#include "example_sul.hpp"

using namespace std;
void ExampleSUL::Reset()
{
    this->curr_state = s0;
}

string ExampleSUL::Step(string symb)
{
    string a = "a";
    string b = "b";
    string c = "c";
    switch (this->curr_state)
    {
    case s0:
    {
        if (a == symb)
        {
            this->curr_state = s1;
            return "x";
        }
        else if (b == symb)
        {
            this->curr_state = s2;
            return "y";
        }
        else if (c == symb)
        {
            return "z";
        }
    }
    break;
    case s1:
    {
        if (a == symb)
        {
            return "z";
        }
        else if (b == symb)
        {
            this->curr_state = s2;
            return "y";
        }
        else if (c == symb)
        {
            return "z";
        }
    }
    break;
    case s2:
    {
        if (a == symb)
        {
            return "z";
        }
        else if (b == symb)
        {
            this->curr_state = s0;
            return "y";
        }
        else if (c == symb)
        {
            return "z";
        }
    }
    break;
    default:
        exit(-1);
        return "sss";
    }
    // Unreachable.
    exit(-1);
    return "sss";
}

ExampleSUL::ExampleSUL(/* args */)
{
}

ExampleSUL::~ExampleSUL()
{
}

ExampleSUL sul = ExampleSUL();

extern "C" void Reset()
{
    sul.Reset();
}

extern "C" string Step(string in)
{
    return sul.Step(in);
}

extern "C" void awesome(char *dest)
{
    string s = string("ss");

    s.copy(dest, s.length(), 0);
    // return string("42");
}
#include <string>
#include <iostream>
#include "basic_io.hpp"

using namespace std;

void ExampleSUL::Reset()
{
    this->curr_state = s0;
}

void ExampleSUL::Step(string symb)
{
    string a = "a";
    string b = "b";
    string c = "c";
    string output = "UNDEF";
    switch (this->curr_state)
    {
    case s0:
    {
        if (a == symb)
        {
            this->curr_state = s1;
            output = "x";
        }
        else if (b == symb)
        {
            this->curr_state = s2;
            output = "y";
        }
        else if (c == symb)
        {
            output = "z";
        }
    }
    break;
    case s1:
    {
        if (a == symb)
        {
            output = "z";
        }
        else if (b == symb)
        {
            this->curr_state = s2;
            output = "y";
        }
        else if (c == symb)
        {
            output = "z";
        }
    }
    break;
    case s2:
    {
        if (a == symb)
        {
            output = "z";
        }
        else if (b == symb)
        {
            this->curr_state = s0;
            output = "y";
        }
        else if (c == symb)
        {
            output = "z";
        }
    }
    break;
    default:
        exit(-1);
    }

    if (output.compare("UNDEF") == 0) // transition was undefined.
    {
        // Unreachable.
        cerr << "UNDEF" << endl;
        exit(-1);
    }
    else
    {
        cout << output << endl;
    }
    return;
}

ExampleSUL::ExampleSUL(/* args */)
{
}

ExampleSUL::~ExampleSUL()
{
}

int main(void)
{
    ExampleSUL sul = ExampleSUL();
    while (true)
    {
        string symb = "";
        getline(cin >> ws, symb);
        if (symb.length() == 0)
        {
            getline(cin >> ws, symb);
        }
        if (symb.compare("RESET") == 0)
        {
            sul.Reset();
        }
        else
        {
            sul.Step(symb);
        }
    }
    return 0;
}
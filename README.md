# Instructions
1. You need to download and build [H-ADS](https://gitlab.science.ru.nl/bharat/hybrid-ads/-/tree/814879b7a01062ac40508131c81f3d1bcff72087), 
and specify the path for the H-ADS executable (called "main" by default in Linux) in ``main.rs`` in the example app.
2. Build this project. 
3. You can run ``cargo run -- --help`` for usage information. Note, the symbols in the (input/output) alphabets are separated by a space. For e.g., "-I a -I b -O o1 -O o2" indicates that the SUL has the input alphabet ``{a,b}`` and output alphabet ``{o1,o2}``.

For example, to run the stdio SUL, you can do:
``cargo run -- -I a -I b -I c -M <path-to-stdio-binary>``